// Server Config
const PORT = 8080;

// MongoDB Config
const dbuser = 'wtg-web';
const dbpassword = 'wtg-web';
const MONGO_URL = 'mongodb://' + dbuser +':' + dbpassword + '@ds055855.mongolab.com:55855/wtg-subpub';

module.exports = {
	PORT: PORT,
	MONGO_URL: MONGO_URL
};