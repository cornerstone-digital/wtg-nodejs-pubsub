var MessagingService = function(serviceUrl) {
	this.socket = io.connect(serviceUrl);

	this.identify = function(data) {
		this.socket.emit('identify', data);
	};

	this.subscribe = function(subs) {
		if(!Array.isArray(subs)) {
			subs = [subs];
		}

		var socket = this.socket;

		subs.forEach(function(sub, index) {
			socket.emit('subscribe', sub.channel);

			socket.on(sub.channel + '-' + sub.action, function(data) {
				sub.callback(data);
			});
		});
	};

	this.unsubscribe = function(channel) {
		this.socket.emit('unsubscribe', channel);
	};

	this.publish = function(data) {
		this.socket.emit('publish', {
			channel: data.channel,
			action: data.action,
			content: data.content
		});
	};
};