const mongoose = require('mongoose');

// create a schema for chat
const MessageSchema = mongoose.Schema({
	created: Date,
	channel: String,
	initiatedBy: String,
	action: String,
	content: mongoose.Schema.Types.Mixed
});

const Message = mongoose.model('Message', MessageSchema);

module.exports = {
	Message: Message,
	MessageSchema: MessageSchema
};