var express = require('express');
var app = module.exports = express();
var Message  = require('../../model/messages').Message;
var APIResponse = require('../apiResponse');

/* MESSAGE API */

/**
 * @api {get} /api/v1/messages getMessages
 * @apiVersion 1.0.0
 * @apiName GetMessages
 * @apiGroup Messages
 *
 * @apiSuccess {Boolean}	success								Was request successful?
 * @apiSuccess {Object}		data								Response data
 * @apiSuccess {Object[]} 	data.messages						List of messages
 * @apiSuccess {Number} 	data.messages._id 					Message id.
 * @apiSuccess {String} 	data.messages.created  				Created date
 * @apiSuccess {String}		data.messages.channel				Message channel
 * @apiSuccess {String}		data.messages.action					Message action
 * @apiSuccess {Object}		data.messages.content				Message content
 * @apiSuccess {String}		data.messages.content.type			Message type
 * @apiSuccess {String}		data.messages.content.message		Message text
 * @apiSuccess {Boolean}	data.messages.content.dismissable	Is message dismissable?
 * @apiSuccess {Number}		data.messages.content.dismissAfter	How long in milliseconds to dismiss after
 * @apiSuccess {Number}		data.messages.__v
 * @apiSuccess {Number}		records								How many records
 *
 * @apiSuccessExample {json} Success-Response:
 * 	{
 * 		success: true
 * 		data: [
 * 			{
 *				_id: "56c63f5ba805546b6cb638c2",
 *				created: "2016-02-18T22:02:03.906Z",
 *				channel: "administrator",
 *				action: "message",
 *				content: {
 *					type: "success",
 *					message: "Test message",
 *					dismissable: true,
 *					dismissAfter: "5000"
 *					},
 *				__v: 0
 *			},
 *			{
 *				_id: "56c5fd6f9621f46156f62a7a",
 *				created: "2016-02-18T17:20:47.009Z",
 *				channel: "global",
 *				content: {
 *					type: "Alert",
 *					level: "info",
 *					message: "You have an info message"
 *					dismissable: true,
 *					dismissAfter: "5000"
 *				},
 *				__v: 0
 *			}
 *		],
 *		records: 2
 *	}
 */

// GET ALL MESSAGES
app.get('/api/v1/messages', function(req, res) {
	Message.find(function(err, messages) {
		if (err)
			res.json(new APIResponse({success: false, data: err}));
		res.json(new APIResponse({data: messages, recordcount: messages.length}));
	});
});

/**
 * @api {post} /api/v1/messages addMessage
 * @apiVersion 1.0.0
 * @apiName AddMessage
 * @apiGroup Messages
 *
 * @apiParam {String}	channel					Channel for the message to be broadcast to
 * @apiParam {String}	action					Action of the broadcasted message
 * @apiParam {Object}	content					Content for the broadcasted message
 * @apiParam {String}	content.type			Message type
 * @apiParam {String}	content.message			Message text
 * @apiParam {Boolean}	content.dismissable		Is message dismissable?
 * @apiParam {Number}	content.dismissAfter	How long in milliseconds to dismiss after
 *
 * @apiSuccessExample {json} Success-Response:
 * 	{
 * 		success: true
 * 		data: [
 * 			{
 *				_id: "56c63f5ba805546b6cb638c2",
 *				created: "2016-02-18T22:02:03.906Z",
 *				channel: "administrator",
 *				action: "message",
 *				content: {
 *					type: "success",
 *					message: "Test message",
 *					dismissable: true,
 *					dismissAfter: "5000"
 *					},
 *				__v: 0
 *			}
 *		],
 *		records: 1
 *	}
 */

// ADD MESSAGE
app.post('/api/v1/messages', function(req, res) {
	var message = new Message({
		created: new Date(),
		channel: req.body.channel,
		action: req.body.action,
		content: request.body.content
	});

	message.save(function(err, message, numEffected) {
		if(err)
			res.json(new APIResponse({success: false, data: err}));

		io.in(message.channel).emit(message.channel + '-' + message.action, message);

		res.json(new APIResponse({data: message, records: numEffected}));
	});
});

/**
 * @api {get} /api/v1/messages/:message_id getMessageById
 * @apiVersion 1.0.0
 * @apiName GetMessageById
 * @apiGroup Messages
 *
 * @apiParam	{String}	message_id						ID of the message
 *
 * @apiSuccess {Boolean}	success								Was request successful?
 * @apiSuccess {Object}		data								Response data
 * @apiSuccess {Object[]} 	data.messages						List of messages
 * @apiSuccess {Number} 	data.messages._id 					Message id.
 * @apiSuccess {String} 	data.messages.created  				Created date
 * @apiSuccess {String}		data.messages.channel				Message channel
 * @apiSuccess {String}		data.messages.action					Message action
 * @apiSuccess {Object}		data.messages.content				Message content
 * @apiSuccess {String}		data.messages.content.type			Message type
 * @apiSuccess {String}		data.messages.content.message		Message text
 * @apiSuccess {Boolean}	data.messages.content.dismissable	Is message dismissable?
 * @apiSuccess {Number}		data.messages.content.dismissAfter	How long in milliseconds to dismiss after
 * @apiSuccess {Number}		data.messages.__v
 * @apiSuccess {Number}		records								How many records
 *
 * @apiSuccessExample {json} Success-Response:
 * 	{
 * 		success: true
 * 		data: {
 *				_id: "56c63f5ba805546b6cb638c2",
 *				created: "2016-02-18T22:02:03.906Z",
 *				channel: "administrator",
 *				action: "message",
 *				content: {
 *					type: "success",
 *					message: "Test message",
 *					dismissable: true,
 *					dismissAfter: "5000"
 *					},
 *				__v: 0
 *		},
 *		records: 1
 *	}
 */

// GET SINGLE MESSAGE
app.get('/api/v1/messages/:message_id', function(req, res){
	Message.findById(req.params.message_id, function(err, message) {
		if (err)
			res.json(new APIResponse({data: err}));
		res.json(new APIResponse({data: message}));
	});
});


/**
 * @api {get} /api/v1/messages/action/:action getMessagesByAction
 * @apiVersion 1.0.0
 * @apiName GetMessagesByAction
 * @apiGroup Messages
 *
 * @apiParam	{String}	action							action to filter by
 *
 * @apiSuccess {Boolean}	success								Was request successful?
 * @apiSuccess {Object}		data								Response data
 * @apiSuccess {Object[]} 	data.messages						List of messages
 * @apiSuccess {Number} 	data.messages._id 					Message id.
 * @apiSuccess {String} 	data.messages.created  				Created date
 * @apiSuccess {String}		data.messages.channel				Message channel
 * @apiSuccess {String}		data.messages.action					Message action
 * @apiSuccess {Object}		data.messages.content				Message content
 * @apiSuccess {String}		data.messages.content.type			Message type
 * @apiSuccess {String}		data.messages.content.message		Message text
 * @apiSuccess {Boolean}	data.messages.content.dismissable	Is message dismissable?
 * @apiSuccess {Number}		data.messages.content.dismissAfter	How long in milliseconds to dismiss after
 * @apiSuccess {Number}		data.messages.__v
 * @apiSuccess {Number}		records								How many records
 *
 * @apiSuccessExample {json} Success-Response:
 * 	{
 * 		success: true
 * 		data: [
 * 			{
 *				_id: "56c63f5ba805546b6cb638c2",
 *				created: "2016-02-18T22:02:03.906Z",
 *				channel: "administrator",
 *				action: "message",
 *				content: {
 *					type: "success",
 *					message: "Test message",
 *					dismissable: true,
 *					dismissAfter: "5000"
 *					},
 *				__v: 0
 *			},
 *			{
 *				_id: "56c5fd6f9621f46156f62a7a",
 *				created: "2016-02-18T17:20:47.009Z",
 *				channel: "global",
 *				content: {
 *					type: "Alert",
 *					level: "info",
 *					message: "You have an info message"
 *					dismissable: true,
 *					dismissAfter: "5000"
 *				},
 *				__v: 0
 *			}
 *		],
 *		records: 2
 *	}
 */

// GET MESSAGES BY ACTION
app.get('/api/v1/messages/action/:action', function(req, res) {
	Message.find({action: req.params.action}, function(err, messages) {
		if (err)
			res.json(new APIResponse({data: err}));
		res.json(new APIResponse({data: messages, records: messages.length}));
	});
});

/**
 * @api {get} /api/v1/messages/channel/:channel getMessagesByChannel
 * @apiVersion 1.0.0
 * @apiName GetMessagesByChannel
 * @apiGroup Messages
 *
 * @apiParam	{String}	channel							channel	 to filter by
 *
 * @apiSuccess {Boolean}	success								Was request successful?
 * @apiSuccess {Object}		data								Response data
 * @apiSuccess {Object[]} 	data.messages						List of messages
 * @apiSuccess {Number} 	data.messages._id 					Message id.
 * @apiSuccess {String} 	data.messages.created  				Created date
 * @apiSuccess {String}		data.messages.channel				Message channel
 * @apiSuccess {String}		data.messages.action					Message action
 * @apiSuccess {Object}		data.messages.content				Message content
 * @apiSuccess {String}		data.messages.content.type			Message type
 * @apiSuccess {String}		data.messages.content.message		Message text
 * @apiSuccess {Boolean}	data.messages.content.dismissable	Is message dismissable?
 * @apiSuccess {Number}		data.messages.content.dismissAfter	How long in milliseconds to dismiss after
 * @apiSuccess {Number}		data.messages.__v
 * @apiSuccess {Number}		records								How many records
 *
 * @apiSuccessExample {json} Success-Response:
 * 	{
 * 		success: true
 * 		data: [
 * 			{
 *				_id: "56c63f5ba805546b6cb638c2",
 *				created: "2016-02-18T22:02:03.906Z",
 *				channel: "administrator",
 *				action: "message",
 *				content: {
 *					type: "success",
 *					message: "Test message",
 *					dismissable: true,
 *					dismissAfter: "5000"
 *					},
 *				__v: 0
 *			},
 *			{
 *				_id: "56c5fd6f9621f46156f62a7a",
 *				created: "2016-02-18T17:20:47.009Z",
 *				channel: "global",
 *				content: {
 *					type: "Alert",
 *					level: "info",
 *					message: "You have an info message"
 *					dismissable: true,
 *					dismissAfter: "5000"
 *				},
 *				__v: 0
 *			}
 *		],
 *		records: 2
 *	}
 */

// GET MESSAGES BY CHANNEL
app.get('/api/v1/messages/channel/:channel', function(req, res) {
	Message.find({channel: req.params.channel}, function(err, messages) {
		if (err)
			res.send(new APIResponse({success: false, data: err}));
		res.json(new APIResponse({data: messages, records: messages.length}));
	});
});