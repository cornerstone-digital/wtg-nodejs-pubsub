var apiResponse = function(params) {
	this.success = params.success || true;
	this.data = params.data || [];
	this.records = params.records || 0;
};

module.exports = apiResponse;