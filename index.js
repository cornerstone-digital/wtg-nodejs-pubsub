var express    = require('express');
var mongoose   = require('mongoose');
var app        = express();
var server     = require('http').Server(app);
var io         = require('socket.io')(server);
var nodemailer = require('nodemailer');
var bodyParser = require('body-parser');
var router     = express.Router;
var Message  = require('./app/model/messages').Message;

var config 	 = require('./app/_config');

app.use(express.static(__dirname + '/app/public'));
app.use(express.static(__dirname + '/app/api/doc'));

mongoose.connect(config.MONGO_URL);

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// allow CORS
app.all('*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	if (req.method == 'OPTIONS') {
		res.status(200).end();
	} else {
		next();
	}
});

// Setup Nodemailer
//var Mailer =

// Default Route
app.get('/', function(req, res) {
	//send the index.html in our public directory
	res.sendFile('index.html');
});

/******************************************
 * API
 ******************************************/
var messageAPI 	= require('./app/api/messages');

app.use(messageAPI);

app.get('/api', function(req, res) {
	//send the index.html in our public directory
	res.sendFile('app/api/doc/index.html', { root: __dirname });
});

//Listen for connection
io.on('connection', function(socket) {
	var defaultChannel = 'global';

	//Listens for identifying of a user
	socket.on('identify', function(data) {
		data.channel = defaultChannel;
		socket.join(defaultChannel);
		socket.join(data.username.toLowerCase());

		var message = new Message({
			created: new Date(),
			channel: data.channel,
			action: 'notification',
			initiatedBy: data.username.toLowerCase(),
			content: {
				title: 'Notification',
				level: 'info',
				dismissable: true,
				message: data.name + ' has just logged in'
			}
		});

		//Tell all those currently logged in that a new user has been logged in
		io.in(message.channel).emit(message.channel, message);

		message.save();
	});

	socket.on('subscribe', function(channel) {
		socket.join(channel);
	});

	socket.on('unsubscribe', function(channel) {
		socket.leave(channel);
	});

	// Listens for published messages
	socket.on('publish', function(data) {
		var message = new Message({
			created: new Date(),
			channel: data.channel,
			action: data.action,
			content: data.content
		});

		io.in(message.channel).emit(message.channel + '-' + message.action, message);

		if(data.emailNotification) {

		}

		message.save();
	});
});

server.listen(process.env.PORT || config.PORT);
console.log('Server connected on port: ' + config.PORT);